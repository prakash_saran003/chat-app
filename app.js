const path = require("path");
const express = require("express");
const bodyParser = require("body-parser");
const mongoose = require("mongoose");
const session = require("express-session");
const MongoDBStore = require("connect-mongodb-session")(session);
const User = require("./models/user");
const multer = require("multer");
const authController = require("./controllers/auth");
const passport = require("passport");
var userProfile;

const MONGODB_URI =
  "mongodb+srv://Prakash:2IQKCuRUsUw9IPkm@cluster0.b8vgv.mongodb.net/chat-app";

const app = express();
const store = new MongoDBStore({
  uri: MONGODB_URI,
  collection: "sessions",
  expires: 1000 * 60 * 60 * 24,
});

const fileStorage = multer.diskStorage({
  destination: (req, file, cb) => {
    cb(null, "profilePics");
  },
  filename: (req, file, cb) => {
    cb(null, Date.now() + "-" + file.originalname);
  },
});

const fileFilter = (req, file, cb) => {
  if (
    file.mimetype === "image/png" ||
    file.mimetype === "image/pg" ||
    file.mimetype === "image/jpeg"
  ) {
    cb(null, true);
  } else {
    req.fileValidationError = "Only .png, .jpg and .jpeg format allowed!";
    return cb(null, false);
  }
};

const authRoutes = require("./routes/auth");

app.use(passport.initialize());
app.use(passport.session());

app.use(bodyParser.json());
app.use(
  multer({ storage: fileStorage, fileFilter: fileFilter }).single("image")
);
app.use(express.static(path.join(__dirname, "public")));
app.use("/profilePics", express.static(path.join(__dirname, "profilePics")));
app.use(
  session({
    secret: "my secret",
    resave: false,
    saveUninitialized: false,
    store: store,
  })
);

const OAuth2Data = require("./google_key.json");

const GOOGLE_CLIENT_ID = OAuth2Data.web.client_id;
const GOOGLE_CLIENT_SECRET = OAuth2Data.web.client_secret;
const REDIRECT_URL = OAuth2Data.web.redirect_uris[0];

app.get("/error", (req, res) => res.json({ msg: "error logging in" }));

const GoogleStrategy = require("passport-google-oauth").OAuth2Strategy;

passport.use(
  new GoogleStrategy(
    {
      clientID: GOOGLE_CLIENT_ID,
      clientSecret: GOOGLE_CLIENT_SECRET,
      callbackURL: REDIRECT_URL,
    },
    function (accessToken, refreshToken, profile, done) {
      userProfile = profile;
      return done(null, userProfile);
    }
  )
);

passport.serializeUser(function (user, cb) {
  cb(null, user);
});

passport.deserializeUser(function (obj, cb) {
  cb(null, obj);
});

app.get(
  "/auth/google",
  passport.authenticate("google", { scope: ["profile", "email"] })
);

app.get(
  "/authenticate/google",
  passport.authenticate("google", { failureRedirect: "/error" }),
  authController.getGoogleAuthComplete
);

app.use(authRoutes);
app.get("/", (req, res) => {
  let loggedInUser = req.session.user;
  console.log("req.session.user 12: ", loggedInUser);

  // if (!loggedInUser) {
  res.sendFile(path.join(__dirname, "views", "index.html"));
  // }
});

// mongoose.set('debug', true);
mongoose
  .connect(MONGODB_URI, { useNewUrlParser: true, useUnifiedTopology: true })
  .then((result) => {
    const server = app.listen(3080);
    const io = require("./socket").init(server);
    io.on("connection", (socket) => {
      // console.log("Client connected : ", socket.id);
      socket.on("new-user-connected", (userId) => {
        // console.log("new-user-connected User ID : ", userId);

        User.findOne({
          _id: mongoose.Types.ObjectId(userId.userId._id),
        }).then((userDoc) => {
          // console.log("[app.js] userDoc : ", userDoc);
          if (userDoc) {
            userDoc.socketId = socket.id;
            userDoc.online = true;
            userDoc.save().then((result) => {
              console.log("SOCKET connection SAVE: ", result);
              io.emit("new-user-online-offline-status", result);
            });
          }
        });
      });

      socket.on("connect", () => {
        console.log("server side connect socket.id", socket.id);
      });

      socket.on("disconnect", () => {
        console.log("server side disconnect socket.id", socket.id);
        User.findOne({
          socketId: socket.id,
        }).then((userDoc) => {
          // console.log("[app.js] userDoc : ", userDoc);
          if (userDoc) {
            userDoc.socketId = "";
            userDoc.online = false;
            userDoc.save().then((result) => {
              console.log("SOCKET disconnect SAVE: ", result);
              io.emit("new-user-online-offline-status", result);
            });
          }
        });
      });
    });
  })
  .catch((err) => {
    console.log(err);
  });
