const mongoose = require("mongoose");

const Schema = mongoose.Schema;

const userChatSchema = new Schema({
  message: { type: String, required: true },
  sender: { type: Schema.Types.ObjectId, required: true, ref: "User" },
  receiver: { type: Schema.Types.ObjectId, required: true, ref: "User" },
  time: { type: Date, required: true },
});

module.exports = mongoose.model("UserChat", userChatSchema);
