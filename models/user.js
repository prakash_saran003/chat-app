const mongoose = require("mongoose");

const Schema = mongoose.Schema;

const userSchema = new Schema({
  name: { type: String, required: true },
  phoneno: { type: String },
  email: { type: String },
  password: { type: String },
  profilePic: { type: String },
  socketId: { type: String },
  paswordResetOtp: { type: Number },
  online: { type: Boolean },
});

module.exports = mongoose.model("User", userSchema);
