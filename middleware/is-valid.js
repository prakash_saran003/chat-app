const { validationResult } = require("express-validator");

module.exports = (req, res, next) => {
  const errors = validationResult(req);
//   console.log("[is-valid.js] validationResult : ", errors);

  let inValidFields = {
    name: false,
    nameMsg: "",
    email: false,
    emailMsg: "",
    phoneno: false,
    password: false,
    passwordMsg: "",
  };
  errors.errors.forEach((elem) => {
    if (elem.param == "user.name" && !inValidFields.name) {
      inValidFields.name = true;
      inValidFields.nameMsg = elem.msg;
    } else if (elem.param == "user.email" && !inValidFields.email) {
      inValidFields.email = true;
      inValidFields.emailMsg = elem.msg;
    } else if (elem.param == "user.phoneno") {
      inValidFields.phoneno = true;
    } else if (
      (elem.param == "user.password" || elem.param == "user.confirmPassword") &&
      !inValidFields.password
    ) {
      inValidFields.password = true;
      inValidFields.passwordMsg = elem.msg;
    }
  });

  if (inValidFields.name) {
    return res.json({
      msg: inValidFields.nameMsg,
    });
  }

  if (inValidFields.email && inValidFields.phoneno) {
    return res.json({
      msg: inValidFields.emailMsg,
    });
  }
  if (inValidFields.password) {
    return res.json({
      msg: inValidFields.passwordMsg,
    });
  }
  req.inValidFields = inValidFields;
  next();
};
