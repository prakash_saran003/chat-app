module.exports = (req, res, next) => {
  if (!req.session.user && !req.session.passport) {
    return res.json({
      msg: "not authenticated",
      googleLoginUrl: "/auth/google",
    });
  }
  next();
};
