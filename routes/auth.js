const express = require("express");

const authController = require("../controllers/auth");
const { body } = require("express-validator");
const isAuth = require("../middleware/is-auth");
const isValid = require("../middleware/is-valid");

const router = express.Router();

router.post(
  "/api/user-signup",
  body("user.name")
    .not()
    .isEmpty()
    .withMessage("Name is required.")
    .isLength({
      min: 3,
    })
    .withMessage("Name must be of 3 characters long.")
    .matches(/^[A-Za-z\s]+$/)
    .withMessage("Name must be alphabetic."),
  body("user.email")
    .isLength({ min: 1 })
    .withMessage("Email or Phone No is required.")
    .isEmail()
    .withMessage("Please provide a valid email or phone no.")
    .normalizeEmail(),
  body("user.phoneno")
    .isLength({ min: 1 })
    .withMessage("Email or Phone No is required.")
    .isMobilePhone()
    .withMessage("Please provide a valid email or phone no."),
  body("user.password")
    .not()
    .isEmpty()
    .withMessage("Password is required.")
    .isLength({
      min: 4,
    })
    .withMessage("Password should be 4 characters long."),
  body("user.confirmPassword").custom((value, { req, loc, path }) => {
    if (value !== req.body.user.password) {
      throw new Error("Passwords don't match.");
    } else return value;
  }),
  isValid,
  authController.postSignup
);

router.post(
  "/api/user-signin",
  body("user.email")
    .isLength({ min: 1 })
    .withMessage("Email or Phone No is required.")
    .isEmail()
    .withMessage("Please provide a valid email or phone no.")
    .normalizeEmail(),
  body("user.phoneno")
    .isLength({ min: 1 })
    .withMessage("Email or Phone No is required.")
    .isMobilePhone()
    .withMessage("Please provide a valid email or phone no."),
  body("user.password")
    .not()
    .isEmpty()
    .withMessage("Password is required.")
    .custom((value, { req, loc, path }) => {
      if (req.body.user.setPassword && value.length < 4) {
        throw new Error("Password should be 4 characters long.");
      } else return value;
    }),
  isValid,
  authController.postLogin
);

router.post(
  "/api/forget-password",
  body("user.email")
    .isLength({ min: 1 })
    .withMessage("Email or Phone No is required.")
    .isEmail()
    .withMessage("Please provide a valid email or phone no.")
    .normalizeEmail(),
  body("user.phoneno")
    .isLength({ min: 1 })
    .withMessage("Email or Phone No is required.")
    .isMobilePhone()
    .withMessage("Please provide a valid email or phone no."),
  isValid,
  authController.postForgetPassword
);

router.post("/api/user-signout", authController.postSignOut);

router.get("/api/users", isAuth, authController.getAllUsers);

router.get("/api/user-loggedIn", isAuth, authController.getCheckLoggedIn);

router.post(
  "/api/send-user-message",
  isAuth,
  authController.postSendUserMessage
);

router.get(
  "/api/chat-messages/:userId/:fetchAfterDate",
  isAuth,
  authController.getChatMessages
);

router.post(
  "/api/update-profile-pic",
  isAuth,
  authController.postUpdateProfilePic
);

module.exports = router;
