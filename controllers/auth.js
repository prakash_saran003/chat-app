const bycrpt = require("bcryptjs");
const User = require("../models/user");
const UserChat = require("../models/userChat");
const io = require("../socket");
const mongoose = require("mongoose");
const otpGenerator = require("otp-generator");
const sgMail = require("@sendgrid/mail");
sgMail.setApiKey(
  "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"
);

const FETCH_CHAT_COUNT = 10;

exports.postSignup = (req, res, next) => {
  const user = req.body.user;
  console.log("Adding user controller = ", user);

  let userFinder;

  if (!req.inValidFields.email) {
    console.log("finding by email");
    userFinder = User.findOne({ email: user.email });
  } else {
    console.log("finding by phoneno");
    userFinder = User.findOne({ phoneno: user.phoneno });
  }

  userFinder
    .then((userDoc) => {
      // console.log("userDoc : ", userDoc);
      if (userDoc) {
        return res.json({ msg: "User already Exists." });
      }
      return bycrpt
        .hash(user.password, 12)
        .then((hashedPassword) => {
          let userToAdd = {
            name: user.name,
            password: hashedPassword,
          };
          if (!req.inValidFields.email) {
            userToAdd.email = user.email;
          } else {
            userToAdd.phoneno = user.phoneno;
          }

          userToAdd = new User(userToAdd);
          return userToAdd.save().then((result) => {
            // console.log("THEN RESULT : ", result);
            res.json({ msg: "success" });
          });
        })
        .catch((err) => {
          console.log("bycrpt hash err : ", err);
        });
    })
    .catch((err) => {
      console.log("user findone err : ", err);
    });
};

exports.postLogin = (req, res, next) => {
  const user = req.body.user;
  console.log("postLogin controller = ", user);

  let userFinder;

  if (!req.inValidFields.email) {
    console.log("finding by email");
    userFinder = User.findOne({ email: user.email });
  } else {
    console.log("finding by phoneno");
    userFinder = User.findOne({ phoneno: user.phoneno });
  }

  userFinder
    .then((userDoc) => {
      // console.log("userDoc : ", userDoc);
      if (!userDoc) {
        return res.json({ msg: "User does not Exists." });
      }
      if (user.setPassword) {
        return bycrpt
          .hash(user.password, 12)
          .then((hashedPassword) => {
            if (user.otp && user.otp != userDoc.paswordResetOtp) {
              return res.json({ msg: "OTP Invalid" });
            }

            userDoc.password = hashedPassword;
            userDoc.paswordResetOtp = undefined;

            return userDoc.save().then((result) => {
              // console.log("THEN RESULT : ", result);
              if (user.otp) {
                return res.json({ msg: "Password Reset Success" });
              }
              res.json({
                msg: "success",
                userData: {
                  _id: userDoc._id,
                  name: userDoc.name,
                  email: userDoc.email,
                  phoneno: userDoc.phoneno,
                  profilePic: userDoc.profilePic,
                },
              });
            });
          })
          .catch((err) => {
            console.log("bycrpt hash err : ", err);
          });
      } else {
        return bycrpt
          .compare(user.password, userDoc.password)
          .then((doMatch) => {
            if (doMatch) {
              // console.log("doMatch : ", doMatch);
              // console.log("req.session : ", req.session);

              if (!req.session.passport && !req.session.user) {
                req.session.user = userDoc;
              } else {
                if (req.session.user) {
                  req.session.user = userDoc;
                } else {
                  req.session.passport.user = userDoc;
                }
              }

              return req.session.save((err) => {
                console.log("req.session.save err : ", err);

                let currUser;
                if (req.session.user) {
                  currUser = req.session.user;
                } else {
                  currUser = req.session.passport.user;
                }

                return res.json({
                  msg: "success",
                  userData: {
                    _id: currUser._id,
                    name: currUser.name,
                    email: currUser.email,
                    phoneno: currUser.phoneno,
                    profilePic: currUser.profilePic,
                  },
                });
              });
            }
            return res.json({ msg: "Password Incorrect" });
          })
          .catch((err) => {
            console.log("bycrpt compare err : ", err);
          });
      }
    })
    .catch((err) => {
      console.log("user findone err : ", err);
    });
};

exports.postSignOut = (req, res, next) => {
  let currUser;
  if (req.session.user) {
    currUser = req.session.user;
  } else if (req.session.passport) {
    currUser = req.session.passport.user;
  } else {
    return res.json({ msg: "Error signing OUT" });
  }

  User.findOne({
    _id: mongoose.Types.ObjectId(currUser._id),
  }).then((userDoc) => {
    if (userDoc) {
      userDoc.socketId = "";
      userDoc.online = false;
      userDoc.save().then((result) => {
        req.session.destroy((err) => {
          console.log(err);
          io.getIO().emit("new-user-online-offline-status", userDoc);
          res.json({ msg: "signed Out" });
        });
      });
    }
  });
};

exports.getAllUsers = (req, res, next) => {
  let currUserId;
  if (req.session.user) {
    currUserId = req.session.user._id;
  } else {
    currUserId = req.session.passport.user._id;
  }

  User.find({ _id: { $ne: currUserId } })
    .select("name phoneno email profilePic online")
    .then((userDocs) => {
      let secondUserId = userDocs.map((elem) => {
        return mongoose.Types.ObjectId(elem._id);
      });

      User.aggregate([
        {
          $match: {
            _id: currUserId, // Loged in user _id
          },
        },
        {
          $lookup: {
            from: "userchats",
            let: { user_id: "$_id" },
            pipeline: [
              {
                $match: {
                  $expr: {
                    $or: [
                      { $eq: ["$receiver", "$$user_id"] },
                      { $eq: ["$sender", "$$user_id"] },
                    ],
                  },
                },
              },
              {
                $sort: { time: -1 },
              },
              {
                $group: {
                  _id: {
                    $concat: [
                      { $toString: { $min: ["$sender", "$receiver"] } },
                      { $toString: { $max: ["$sender", "$receiver"] } },
                    ],
                  },
                  sender: { $first: "$sender" },
                  receiver: { $first: "$receiver" },
                  message: { $first: "$message" },
                  time: { $first: "$time" },
                },
              },
              // {
              //   $lookup: {
              //     from: "users",
              //     localField: "sender",
              //     foreignField: "_id",
              //     as: "sender",
              //   },
              // },
              // {
              //   $lookup: {
              //     from: "users",
              //     localField: "receiver",
              //     foreignField: "_id",
              //     as: "receiver",
              //   },
              // },
            ],
            as: "latestMsg",
          },
        },
        { $unwind: "$latestMsg" },
        { $sort: { "latestMsg.time": -1 } },
      ]).then((experiment) => {
        let newUserDocs1 = userDocs.map((elem) => {
          let userID = elem._doc._id.toString();
          // console.log("userID DOCCCC : ", userID);
          let lmsg = experiment.find((ele) => {
            if (
              ele.latestMsg.sender.toString() == userID ||
              ele.latestMsg.receiver.toString() == userID
            ) {
              return true;
            }
            return false;
          });
          // console.log("userID DOCCCC lmsg: ", lmsg);
          if (lmsg) {
            return { ...elem._doc, lastMsg: lmsg.latestMsg };
          }
          return { ...elem._doc };
        });

        newUserDocs1.sort(function (a, b) {
          if (a.lastMsg && b.lastMsg) {
            var keyA = new Date(a.lastMsg.time),
              keyB = new Date(b.lastMsg.time);
            // Compare the 2 dates
            if (keyA < keyB) return 1;
            if (keyA > keyB) return -1;
          } else if (a.lastMsg) {
            return -1;
          } else if (b.lastMsg) {
            return 1;
          }

          return 0;
        });

        res.json({ msg: "success", usersData: newUserDocs1 });
      });
    });
};

exports.postSendUserMessage = (req, res, next) => {
  let chat = req.body.message;

  let currUser;
  if (req.session.user) {
    currUser = req.session.user;
  } else {
    currUser = req.session.passport.user;
  }
  chat.sender = currUser._id;
  console.log("chat : ", chat);

  const chatToAdd = new UserChat(chat);
  return chatToAdd
    .save()
    .then((result) => {
      // console.log("THEN RESULT : ", result);
      // io.getIO().emit("chat-added", chat);
      chat.sender = {
        _id: currUser._id,
        name: currUser.name,
        phoneno: currUser.phoneno,
        email: currUser.email,
      };
      User.findOne({
        _id: mongoose.Types.ObjectId(chat.receiver),
      })
        .select("name phoneno email socketId")
        .then((userDoc) => {
          chat.receiver = userDoc;
          chat.sentByMe = true;

          if (userDoc.socketId && userDoc.socketId != "") {
            console.log("emitting to receiver...");
            let tmpchat = { ...chat };
            tmpchat.sentByMe = false;
            io.getIO().to(userDoc.socketId).emit("new-message", tmpchat);
          }
          res.json({ msg: "success", chat: chat });
        })
        .catch((err) => {
          console.log(err);
        });
    })
    .catch((err) => {
      console.log(err);
    });
};

exports.getChatMessages = (req, res, next) => {
  let secondUserId = req.params.userId;
  let fetchAfterDate = req.params.fetchAfterDate;

  let currUser;
  if (req.session.user) {
    currUser = req.session.user;
  } else {
    currUser = req.session.passport.user;
  }

  UserChat.find({
    $or: [
      {
        sender: currUser._id,
        receiver: mongoose.Types.ObjectId(secondUserId),
      },
      {
        receiver: currUser._id,
        sender: mongoose.Types.ObjectId(secondUserId),
      },
    ],
    time: { $lt: new Date(fetchAfterDate) },
  })
    .limit(FETCH_CHAT_COUNT)
    .sort({ time: -1 })
    .populate({ path: "sender", select: "name phoneno email" })
    .populate({ path: "receiver", select: "name phoneno email" })
    .then((chats) => {
      chats.sort(function (a, b) {
        var keyA = new Date(a.time),
          keyB = new Date(b.time);
        if (keyA < keyB) return 1;
        if (keyA > keyB) return -1;
        return 0;
      });

      res.json({
        msg: "success",
        chats: chats.map((elm) => {
          if (elm._doc.sender._id.toString() == currUser._id.toString()) {
            return { ...elm._doc, sentByMe: true };
          }
          return { ...elm._doc, sentByMe: false };
        }),
      });
    })
    .catch((err) => {
      console.log(err);
    });
};

exports.postUpdateProfilePic = (req, res, next) => {
  const data = req.file;
  const fileValidationError = req.fileValidationError;

  console.log("fileValidationError : ", fileValidationError);
  if (fileValidationError) {
    res.json({
      msg: fileValidationError,
    });
  } else {
    let currUser;
    if (req.session.user) {
      currUser = req.session.user;
    } else {
      currUser = req.session.passport.user;
    }

    User.findOne({ _id: currUser._id })
      .then((userDoc) => {
        if (userDoc) {
          console.log("userDoc : ", userDoc);
          userDoc.profilePic = data.path;
          userDoc.save().then((result) => {
            // console.log("data : ", data);

            if (req.session.user) {
              req.session.user.profilePic = data.path;
            } else {
              req.session.passport.user.profilePic = data.path;
            }

            req.session.save();
            res.json({
              msg: "success",
              imageUrl: data.path,
            });
          });
        }
      })
      .catch((err) => {
        console.log(err);
      });
  }
};

exports.getCheckLoggedIn = (req, res, next) => {
  console.log("req.SESSION getCheckLoggedIn: ", req.session);

  let currUser;
  if (req.session.user) {
    currUser = req.session.user;
    res.json({
      msg: "loggedIn",
      userData: {
        _id: currUser._id,
        name: currUser.name,
        phoneno: currUser.phoneno,
        profilePic: currUser.profilePic,
        email: currUser.email,
      },
      googleLoginUrl: "/auth/google",
    });
  } else {
    currUser = req.session.passport.user;
    User.findOne({ email: currUser.emails[0].value }).then((userDoc) => {
      let msg = "loggedIn";
      if (!userDoc.password) {
        msg = "password-not-set";
      }
      res.json({
        msg: msg,
        userData: {
          _id: currUser._id,
          name: currUser.name,
          phoneno: currUser.phoneno,
          profilePic: currUser.profilePic,
          email: currUser.email,
        },
        googleLoginUrl: "/auth/google",
      });
    });
  }
};

exports.getGoogleAuthComplete = (req, res) => {
  let user = req.session.passport.user;

  User.findOne({ email: user.emails[0].value }).then((userDoc) => {
    console.log("getGoogleAuthComplete userDoc : ", userDoc);
    if (userDoc) {
      req.session.passport.user._id = userDoc._id;
      req.session.passport.user.name = userDoc.name;
      req.session.passport.user.email = userDoc.email;
      req.session.passport.user.profilePic = userDoc.profilePic;
      return req.session.save((err) => {
        console.log("req.session.save err : ", err);
        res.redirect("/");
      });
    } else {
      const userToAdd = new User({
        name: user.displayName,
        email: user.emails[0].value,
        profilePic: user.photos[0].value,
      });
      userToAdd.save().then((result) => {
        console.log("THEN RESULT : ", result);
        req.session.passport.user._id = result._id;
        req.session.passport.user.name = result.name;
        req.session.passport.user.email = result.email;
        req.session.passport.user.profilePic = result.profilePic;
        return req.session.save((err) => {
          console.log("req.session.save err : ", err);
          res.redirect("/");
        });
      });
    }
  });
};

exports.postForgetPassword = (req, res, next) => {
  let user = req.body.user;
  console.log("postForgetPassword : ", user);

  if (!req.inValidFields.email) {
    console.log("finding by email");
    userFinder = User.findOne({ email: user.email });
  } else {
    console.log("finding by phoneno");
    return res.json({
      msg: "Password Reset on phone no is not implemented yet.",
    });
    // userFinder = User.findOne({ phoneno: user.phoneno });
  }

  userFinder
    .then((userDoc) => {
      console.log("userDoc : ", userDoc);
      if (!userDoc) {
        return res.json({ msg: "User does not Exists." });
      }
      let otp = otpGenerator.generate(6, {
        upperCase: false,
        alphabets: false,
        specialChars: false,
      });
      console.log("otp : ", otp);
      if (otp) {
        userDoc.paswordResetOtp = otp;
        userDoc.save().then((result) => {
          const msg = {
            to: userDoc.email,
            from: "prakashchoudhary0141@gmail.com",
            subject: "Password Reset OTP",
            html:
              "<p>Here is your password reset otp : <strong>" +
              otp +
              "</strong>.</p>",
          };
          sgMail.send(msg).then(
            (result) => {
              console.log("mail send SUCCESS : ", result);
              res.json({ msg: "success" });
            },
            (error) => {
              console.error(error);
              res.json({ msg: "Error sending OTP." });
              if (error.response) {
                console.error(error.response.body);
              }
            }
          );
        });
      }
    })
    .catch((err) => {
      console.log("getGoogleAuthComplete err : ", err);
    });
};

// .then((result) => {})
// .catch((err) => {
//   console.log("getGoogleAuthComplete err : ", err);
// });
